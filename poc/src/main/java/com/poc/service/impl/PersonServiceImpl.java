package com.poc.service.impl;

import com.poc.db.repository.PersonRepository;
import com.poc.dto.PersonDTO;
import com.poc.service.PersonService;
import com.poc.service.mapper.PersonDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;
    private final PersonDTOMapper mapper;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, PersonDTOMapper mapper) {
        this.personRepository = personRepository;
        this.mapper = mapper;
    }

    @Override
    public List<PersonDTO> findAll() {
        return mapper.toDtos(personRepository.findAll());
    }

    @Override
    public List<PersonDTO> findAllByName(String name) {
        return mapper.toDtos(personRepository.findAllByName(name));
    }

    @Override
    public PersonDTO findById(int id) {
        return mapper.toDto(personRepository.findById(id));
    }
}
