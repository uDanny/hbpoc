package com.poc.service.mapper;


import com.poc.dto.PersonDTO;
import com.poc.model.Person;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonDTOMapper {
    Person toModel(PersonDTO source);

    List<Person> toModels(List<PersonDTO> source);

    List<PersonDTO> toDtos(List<Person> source);

    PersonDTO toDto(Person destination);
}
