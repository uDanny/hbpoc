package com.poc.service;

import com.poc.dto.PersonDTO;

import java.util.List;

public interface PersonService {
    List<PersonDTO> findAll();

    List<PersonDTO> findAllByName(String name);

    PersonDTO findById(int id);
}
