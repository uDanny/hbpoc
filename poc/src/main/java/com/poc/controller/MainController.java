package com.poc.controller;

import com.poc.dto.PersonDTO;
import com.poc.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MainController {

    private final PersonService personService;

    @Autowired
    public MainController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/findAllPersons", method = RequestMethod.GET)
    public List<PersonDTO> findAllPersons() {
        return personService.findAll();
    }

    @RequestMapping(value = "/findAllPersonsByName/{name}", method = RequestMethod.GET)
    public List<PersonDTO> findAllPersonsByName(@PathVariable("name") String name) {
        return personService.findAllByName(name);
    }

    @RequestMapping(value = "/findPersonById", method = RequestMethod.GET)
    public PersonDTO findPersonById(@RequestParam("id") int id) {
        return personService.findById(id);
    }
}
