package com.poc.db.repository;

import com.poc.model.Person;

import java.util.List;

public interface PersonRepository {
    List<Person> findAll();

    List<Person> findAllByName(String name);

    Person findById(int id);
}
