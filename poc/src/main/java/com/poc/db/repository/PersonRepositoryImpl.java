package com.poc.db.repository;

import com.poc.db.mapper.PersonMapper;
import com.poc.model.Person;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {
    private final PersonMapper personMapper;
    private final SqlSession sqlSession; // should be used only for xml based sql queries approach

    @Autowired
    public PersonRepositoryImpl(PersonMapper personMapper, SqlSession sqlSession) {
        this.personMapper = personMapper;
        this.sqlSession = sqlSession;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> findAll() {
        //xml based sql query
        return sqlSession.selectList("findAllPersons");
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> findAllByName(String name) {
        //xml based sql query
        return sqlSession.selectList("findAllPersonsByName", name);
    }

    @Override
    @Transactional(readOnly = true)
    public Person findById(int id) {
        //annotation based sql
        return personMapper.findPersonsById(id);
    }
}
