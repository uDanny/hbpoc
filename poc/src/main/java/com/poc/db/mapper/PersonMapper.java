package com.poc.db.mapper;

import com.poc.model.Person;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface PersonMapper {
    @Select("select id, name from persons where id = #{id}")
    Person findPersonsById(@Param("id") int id);
}
